# Cars
Proyecto para entrenamiento.

### Tecnologías
* Maven 3.5 
* Spring Boot 2.3.1
* JPA 2.1
* H2 Database (in-memory)

### Modelo
![alt text](model.png)

### Base de datos
El proyecto utiliza una base de datos H2 en memoria. Una vez iniciado el proyecto, se puede acceder a la consola H2 desde [aquí](http://localhost:8080/h2-console).

### Tareas
1. Clonar el repositorio.
2. Crear un nuevo branch partiendo de **master** con el nombre **<yyyymmdd_apellido>**.
3. Iniciar el proyecto con el comando **mvn spring-boot:run**.
4. Verificar que la aplicación inició correctamente consultando la ruta **http://localhost:8080/cars** con el método GET.
5. Mapear las entidades que aún no estén mapeadas en el package **com.mercel.cars.model**, respetando las relaciones entre ellas.
6. Crear la capacidad REST (controller, entity y repository) para la entidad **Track** basándose en CarController y CarRepository.
7. Crear la capacidad REST para la entidad **Session**, con un servicio expuesto en la ruta **http://localhost:8080/sessions/{id}**, que retorne toda la información disponible de la sesión (carreras, resultados, pilotos y autos). La estructura esperada será la siguiente:
```
{
    "id": 1,
    "name": "Training Session",
    "races": [
        {
            "id": 4,
            "track": {
                "id": 4,
                "name": "Supermarket"
            },
            "results": [
                {
                    "id": 44,
                    "driver": {
                        "id": 1,
                        "name": "Horrible"
                    },
                    "car": {
                        "id": 1,
                        "name": "Toyeca",
                        "type": {
                            "id": 1,
                            "name": "Rookie"
                        }
                    },
                    "time": 66.0
                },
                ...
            ]
        },
        ...
    ]
}
```
8. Crear un servicio **"getScoresBySession"** que disponibilizado en la ruta **http://localhost:8080/sessions/{id}/scores** que debe retornar una lista ordenada con el puntaje acumulado de cada piloto en todas las carreras de la sesión. Para calcular el mismo, se asignan 10 puntos para el ganador de cada carrera, 5 para el segundo y 0 para el resto.
9. Pushear el branch creado al repositorio de origen.